# react-native-pda-scan

pda

## Installation

```sh
npm install react-native-pda-scan
```

## Usage

```js
import { multiply } from "react-native-pda-scan";

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
