import * as React from 'react';
import { StyleSheet, View, Text, DeviceEventEmitter, NativeEventEmitter } from 'react-native';

export default function App() {
  const [result, setResult] = React.useState([]);

  React.useEffect(() => {

  DeviceEventEmitter.addListener('onPdaScan', function (barcode) {
    setResult(prev => [barcode, ...prev])
  });
  
  }, []);

  return (
    <View style={styles.container}>
      <Text>Barcodes</Text>
      {
        result.map((barcode, i) => {
          return (
            <View key={i}>
            <Text>{barcode}</Text>
            </View>
            )
        })
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
});
