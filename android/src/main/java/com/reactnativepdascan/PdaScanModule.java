package com.reactnativepdascan;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;
import java.util.Map;
import java.util.HashMap;
import android.content.Intent;
import android.os.Bundle;
import java.util.Set;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;

@ReactModule(name = PdaScanModule.NAME)
public class PdaScanModule extends ReactContextBaseJavaModule {
    public static final String NAME = "PdaScan";

    private static final String XM_SCAN_ACTION = "scan.rcv.message";

    public static ReactApplicationContext reactContext;

   
    private final BroadcastReceiver scanReceiver = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
          
        String barcode = intent.getStringExtra("barcodeData");
        Log.d("ReactNativeJS",  barcode);

        sendEvent(getReactApplicationContext(), "onPdaScan", barcode);
      }
    };

    private void sendEvent(ReactContext reactContext, String eventName, String barcode) {
      getReactApplicationContext().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName,
          barcode);
    }

     public PdaScanModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        IntentFilter xmIntentFilter = new IntentFilter();
        xmIntentFilter.addAction(XM_SCAN_ACTION);
        xmIntentFilter.setPriority(Integer.MAX_VALUE);
        getReactApplicationContext().registerReceiver(scanReceiver, xmIntentFilter);
    }


    @Override
    @NonNull
    public String getName() {
        return NAME;
    }

    public static native int nativeMultiply(int a, int b);
}
